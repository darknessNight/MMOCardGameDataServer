package com.MmoCardGame.DataServer.cards.cardsCollection;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.CardModel;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardsFromJsonLoader implements CardsLoader {

    private static Logger logger = LoggerFactory.getLogger(CardsFromJsonLoader.class);

    @Autowired
    private ApplicationContext appContext;

    private String filePath;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    public CardsFromJsonLoader(@Qualifier("cardsJsonFilePath") String filePath){
        this.filePath = filePath;
    }

    public List<Card> loadCards(){
        String fileContent = tryReadFile(filePath);
        List<CardModel> cardModels = new ArrayList<>();
        try {
            cardModels = mapper.readValue(fileContent, new TypeReference<List<CardModel>>(){});
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return cardModels
                .stream()
                .map(model -> {
                    Card card = new Card();
                    card.setId((long) model.getId());
                    card.setType(CardTypes.translateModelCardTypes(model.getType()));
                    return card;
                }).collect(Collectors.toList());
    }

    private String tryReadFile(String filePath){
        List<String> lines= new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            logger.error("Fatal error. Couldn't load cards from file. File path: {}", filePath);
            logger.error("Application will be closed now.");
            logger.error(e.getMessage(), e);
            int exitCode = SpringApplication.exit(appContext);
            System.exit(exitCode);
        }
        StringBuilder result = new StringBuilder();
        lines.forEach(result::append);
        return result.toString();
    }

}

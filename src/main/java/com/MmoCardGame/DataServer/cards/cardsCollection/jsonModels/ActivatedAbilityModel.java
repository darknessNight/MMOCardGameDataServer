package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import lombok.Data;

@Data
public class ActivatedAbilityModel {
    private int id;
    private String description;
    private ActivatedAbilityCostModel cost;
}

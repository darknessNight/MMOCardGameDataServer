package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import lombok.Data;

@Data
public class StatisticsModel {
    private int attack;
    private int defence;
}

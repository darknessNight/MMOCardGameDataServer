package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums;

public enum CardModelSubtype {
    Basic, Legendary
}

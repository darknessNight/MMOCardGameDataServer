package com.MmoCardGame.DataServer.cards.configuration;

public class CardsConsts {
    public static final String JSON_CARDS_FILE_PATH = "data/cards.json";
    public static final int NUMBER_OF_CARDS_IN_SMALL_PACKET = 3;
    public static final int NUMBER_OF_CARDS_IN_MEDIUM_PACKET = 5;
    public static final int NUMBER_OF_CARDS_IN_BIG_PACKET = 10;

    public static final int MIN_CARDS_IN_DECK = 45;
    public static final int MIN_LINK_CARDS = 8;
}

package com.MmoCardGame.DataServer.cards;

import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface DecksRepository extends JpaRepository<Deck, Long>{

    @Query("select d from DECKS d join fetch d.cards where d.name = ?2 and d.user.login = ?1")
    @Modifying
    Deck getUserDeckWithCards(String login, String deckName);
}

package com.MmoCardGame.DataServer.cards;

import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums.CardModelType;

public enum CardTypes {
   LINK, OTHER;

   public static CardTypes translateModelCardTypes(CardModelType modelType){
      if(modelType.equals(CardModelType.Link)){
         return LINK;
      } else {
         return OTHER;
      }
   }
}

package com.MmoCardGame.DataServer.cards.cardsEntities;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "CARDS")
@Data
@NoArgsConstructor
@JsonIgnoreProperties({ "decks", "users" })
public class Card {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "id.cardId")
    private List<UserCard> users = new ArrayList<>();

    @OneToMany(mappedBy = "id.cardId")
    private List<DeckCard> decks = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private CardTypes type;

    public Card(CardTypes type){
        this.type = type;
    }

}

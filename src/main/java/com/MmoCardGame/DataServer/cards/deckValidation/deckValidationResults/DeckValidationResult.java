package com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults;

import lombok.Data;

@Data
public class DeckValidationResult {

    private DeckValidationResults result;
    private String message;

    public DeckValidationResult(DeckValidationResults result){
        this.result = result;
        this.message = DeckValidationResults.getMessage(result);
    }

}

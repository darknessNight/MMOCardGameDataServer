package com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults;

import com.MmoCardGame.DataServer.exceptions.NotExistingDeckValidationResultException;

public enum DeckValidationResults {
    NOT_ENOUGH_CARDS, TOO_MANY_CARDS_OF_THIS_KIND, NOT_ENOUGH_LINK_CARDS, DECK_IS_VALID;

    public static String getMessage(DeckValidationResults result){
        if(result.equals(NOT_ENOUGH_CARDS)) {
            return "Not enough cards of these type. Add more.";
        } else if(result.equals(TOO_MANY_CARDS_OF_THIS_KIND)){
            return "More cards in deck then user has.";
        } else if(result.equals(DECK_IS_VALID)){
            return "Deck is valid.";
        } else if(result.equals(NOT_ENOUGH_LINK_CARDS)){
            return "Not enough cards of link type.";
        }

        throw new NotExistingDeckValidationResultException(result.toString());
    }
}

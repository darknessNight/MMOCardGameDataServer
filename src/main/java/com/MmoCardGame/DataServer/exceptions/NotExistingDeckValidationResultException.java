package com.MmoCardGame.DataServer.exceptions;

public class NotExistingDeckValidationResultException extends RuntimeException {
    public NotExistingDeckValidationResultException(String s) {
        super(s);
    }
}

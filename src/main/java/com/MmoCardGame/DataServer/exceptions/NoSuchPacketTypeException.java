package com.MmoCardGame.DataServer.exceptions;

import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;

public class NoSuchPacketTypeException extends RuntimeException{
    public NoSuchPacketTypeException(PacketsTypes type) {
        super(type.toString());
    }
}

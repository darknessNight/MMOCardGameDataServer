package com.MmoCardGame.DataServer.users.resetPasswordTokens;

import lombok.Data;

import javax.validation.constraints.Future;
import javax.validation.constraints.Pattern;
import java.util.Calendar;
import java.util.Date;

@Data
public class Token {

    @Pattern(regexp="[\\d]{6}", message = TokensSettings.INVALID_TOKEN_MESSAGE)
    private String token;

    private Date creationDate;

    @Future(message = TokensSettings.TOKEN_EXPIRED_MESSAGE)
    private Date expirationDate;

    private String userLogin;

    public Token(String token, Date creationDate, String userLogin){
        this.token = token;
        this.creationDate = creationDate;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationDate);
        calendar.add(Calendar.DATE, TokensSettings.TOKEN_EXPIRATION_DAYS);
        this.expirationDate = calendar.getTime();
        this.userLogin = userLogin;
    }

}

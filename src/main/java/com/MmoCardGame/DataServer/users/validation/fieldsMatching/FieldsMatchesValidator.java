package com.MmoCardGame.DataServer.users.validation.fieldsMatching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class FieldsMatchesValidator implements ConstraintValidator<ValidFieldsMatching, Object> {

    private static final Logger logger = LoggerFactory.getLogger(FieldsMatchesValidator.class);

    private String firstFieldName;
    private String secondFieldName;
    private String message;

    @Override
    public void initialize(ValidFieldsMatching validFields) {
        this.firstFieldName = validFields.firstFieldName();
        this.secondFieldName = validFields.secondFieldName();
        this.message = validFields.message();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {

        boolean valid = isValid(object);

        if (!valid){
            constraintValidatorContext.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(firstFieldName)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }


        return valid;
    }

    public boolean isValid(Object object) {
        boolean valid;
        Object firstValue = getFieldValue(object, firstFieldName);
        Object secondValue = getFieldValue(object, secondFieldName);
        valid = areValid(firstValue, secondValue);
        logger.info("Result of validating values of two fields: {}. {} : {}, {} result : {}",
                firstFieldName, firstValue, secondFieldName, secondValue, valid);
        return valid;
    }

    private boolean areValid(Object firstValue, Object secondValue) {
        return (firstValue == null && secondValue == null) || (firstValue != null && firstValue.equals(secondValue));
    }

    private Object getFieldValue(Object object, String fieldName) {
        Field field;
        Object result = null;
        try {
            field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            result = field.get(object);
        } catch (NoSuchFieldException | IllegalAccessException ignore) {
        }
        return result;
    }

    public void setFirstFieldName(String firstFieldName) {
        this.firstFieldName = firstFieldName;
    }

    public void setSecondFieldName(String secondFieldName) {
        this.secondFieldName = secondFieldName;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.MmoCardGame.DataServer.users.validation.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator implements ConstraintValidator<ValidLogin, String> {
    private static final Logger logger = LoggerFactory.getLogger(LoginValidator.class);

    private static final String LOGIN_PATTERN = "^[A-Z0-9]{3,20}$";

    @Override
    public void initialize(ValidLogin validLogin) {
    }

    @Override
    public boolean isValid(String login, ConstraintValidatorContext context) {
        if(login == null) {
            logger.warn("Attempt of validating null login.");
            return false;
        }
        boolean loginValid = isLoginValid(login);
        logger.info("Login: {} validation result: {}", login, loginValid);
        return loginValid;
    }

    private boolean isLoginValid(String email){
        Pattern pattern = Pattern.compile(LOGIN_PATTERN, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}

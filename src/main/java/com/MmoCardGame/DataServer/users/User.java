package com.MmoCardGame.DataServer.users;

import com.MmoCardGame.DataServer.REST.jsonMessages.CardWithAmount;
import com.MmoCardGame.DataServer.REST.jsonMessages.RegistrationData;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import com.MmoCardGame.DataServer.cards.cardsEntities.UserCard;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "USERS")
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"cards","decks", "numberOfCardPackets"})
@JsonIgnoreProperties({ "decks", "cards" })
public class User {

    private static final Logger logger = LoggerFactory.getLogger(User.class);

    @Id
    @GeneratedValue
    private Long id;

    private String login;

    @Column(length = 60)
    private String password;

    private String email;

    private String selectedDeckName;

    private String role;

    @ElementCollection
    @CollectionTable(name="USERS_PACKETS")
    @MapKeyColumn(name = "PACKET_TYPE")
    @MapKeyEnumerated(EnumType.STRING)
    @Column(name ="PACKETS_QUANTITY")
    private Map<PacketsTypes, Integer> numberOfCardPackets = new HashMap<>();

    private int money;

    @OneToMany(mappedBy = "id.userId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<UserCard> cards = new HashSet<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Deck> decks = new HashSet<>();

    public User(String login, String password, String email, String role){
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = "ROLE_" + role;
        for(PacketsTypes type : PacketsTypes.values()){
            numberOfCardPackets.put(type, 0);
        }
        money = 1000;
    }

    public User(String login, String password, String email){
        this(login, password, email, "USER");
    }

    public User(RegistrationData data){
        this(data.getLogin(), data.getPassword(), data.getEmail());
    }

    public int getAmountOfCard(long id){
        int amountInUsersCollection = 0;
        for(UserCard userCard : cards){
            if(userCard.getCard().getId().equals(id)){
                amountInUsersCollection = userCard.getAmount();
                break;
            }
        }
        return amountInUsersCollection;
    }

    public Deck getDeck(String deckName){
        for(Deck d : decks){
            if(d.getName().equals(deckName))
                return d;
        }
        return null;
    }

    public boolean hasDeck(String deckName){
        return getDeck(deckName) != null;
    }

    public Deck getSelectedDeck(){
        return getDeck(selectedDeckName);
    }

    public List<CardWithAmount> getCardsWithAmounts(){
        return getCards()
                .stream()
                .map(userCard ->
                        new CardWithAmount(userCard.getCard().getId(), userCard.getAmount())
                    )
                .collect(Collectors.toList());
    }

    public synchronized void addPacket(PacketsTypes packetType){
        Integer oldValue = numberOfCardPackets.get(packetType);
        numberOfCardPackets.replace(packetType, oldValue+1);
    }

    public synchronized void removePacket(PacketsTypes packetType){
        Integer oldValue = numberOfCardPackets.get(packetType);
        numberOfCardPackets.replace(packetType, oldValue-1);
    }

    public void addDeck(Deck deck){
        decks.add(deck);
        deck.setUser(this);
    }

    public void addCard(Card card){
        UserCard userCard = tryGetUserCardRelation(card);
        logger.info("Added card: {} user: {}", card.getId(), login);
        if(userCard != null) {
            userCard.increaseAmount();
            logger.info("Increased cards amount in userCard relation. cardId: {}, user: {}, new amount: {}", card.getId(), login, userCard.amount);
        } else {
            addNewUserCardEntity(card);
            logger.info("Created new userCard relation. cardId: {}, user: {}", card.getId(), login);
        }
    }

    private UserCard tryGetUserCardRelation(Card newCard) {
        UserCard userCard = null;
        for(UserCard relation : cards){
            if(Objects.equals(relation.getCard().getId(), newCard.getId())){
                userCard = relation;
            }
        }
        return userCard;
    }

    private void addNewUserCardEntity(Card newCard) {
        UserCard userCard = new UserCard(this, newCard);
        getCards().add(userCard);
        newCard.getUsers().add(userCard);
    }

    public void setNumberOfCardsPacket(PacketsTypes type, int number){
        numberOfCardPackets.replace(type, number);
    }

    public void decreaseMoney(int amount) {
        money -= amount;
    }

    public void increaseMoney(int by) {
        money += by;
    }

    public void removeDeck(String deckName) {
        Deck deck = getDeck(deckName);
        decks.remove(deck);
        deck.setUser(null);
    }

}

package com.MmoCardGame.DataServer.security.configuration;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class UserPathLoginChecker {

    public boolean check(Authentication authentication, String pathLogin){
        String authenticatedName = authentication.getName();
        return pathLogin.equals(authenticatedName);
    }

}

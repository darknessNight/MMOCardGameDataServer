package com.MmoCardGame.DataServer.utilFunctionality.emailServerConfiguration;

import lombok.Data;

@Data
public class EmailServerConfiguration {

    private String user;
    private String host;
    private String password;

}

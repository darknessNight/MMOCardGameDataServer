package com.MmoCardGame.DataServer.utilFunctionality.emailServerConfiguration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

@Data
@Configuration
@PropertySource("classpath:mail.properties")
public class EmailServerConfigurationLoader {

    @Autowired
    private Environment environment;

    @Bean
    public EmailServerConfiguration emailServerConfiguration(){
        EmailServerConfiguration serverConfiguration = new EmailServerConfiguration();
        serverConfiguration.setHost(environment.getProperty("mail.host"));
        serverConfiguration.setPassword(environment.getProperty("mail.password"));
        serverConfiguration.setUser(environment.getProperty("mail.user"));
        return serverConfiguration;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}

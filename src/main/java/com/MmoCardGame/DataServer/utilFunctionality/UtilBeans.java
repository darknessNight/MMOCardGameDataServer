package com.MmoCardGame.DataServer.utilFunctionality;

import com.MmoCardGame.DataServer.utilFunctionality.emailServerConfiguration.EmailServerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilBeans {

    @Autowired
    private EmailServerConfiguration emailServerConfiguration;

    @Bean
    public EmailSender emailSender(){
        return new EmailSender(emailServerConfiguration);
    }


}

package com.MmoCardGame.DataServer.utilFunctionality;

import com.MmoCardGame.DataServer.utilFunctionality.emailServerConfiguration.EmailServerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailSender {

    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

    private EmailServerConfiguration configuration;

    private Session session;

    @Autowired
    public EmailSender(EmailServerConfiguration configuration){
        this.configuration = configuration;
        Properties props = prepareProperties();
        session = createSession(props);
    }

    private Properties prepareProperties() {
        Properties props = new Properties();
        props.put("email.smtp.host", configuration.getHost());
        props.put("email.smtp.auth", "true");
        logProperties();
        return props;
    }

    private void logProperties() {
        logger.info("Email properties:");
        logger.info("Host: {}", configuration.getHost());
        logger.info("Use auth: true");
        logger.info("User: {}", configuration.getUser());
        logger.info("Password: {}", configuration.getPassword());
    }

    private Session createSession(Properties props) {
        return Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        logger.info("Email sender session started.");
                        return new PasswordAuthentication(configuration.getUser(), configuration.getPassword());
                    }
                });
    }

    public void sendMessage(String to, String subject, String body){
        try {
            MimeMessage message = composeMessage(to, subject, body);
            Transport.send(message);
            logEmailData(to, subject, body);
        } catch (MessagingException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void logEmailData(String to, String subject, String body) {
        logger.info("Sending email.");
        logger.info("To: {}.", to);
        logger.info("Subject: {}", subject);
        logger.info("Body: {}", body);
    }

    private MimeMessage composeMessage(String to, String subject, String body) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(configuration.getUser()));
        message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
        message.setSubject(subject);
        message.setText(body);
        return message;
    }

}

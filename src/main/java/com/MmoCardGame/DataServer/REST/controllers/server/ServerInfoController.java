package com.MmoCardGame.DataServer.REST.controllers.server;

import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.appConfiguration.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ServerInfoController {

    private static final Logger logger = LoggerFactory.getLogger(ServerInfoController.class);

    @Autowired
    private AppProperties appProperties;

    @GetMapping(ApiPaths.SERVER_URL + "/canConnect")
    public ResponseEntity hello(){
        logger.info(ResponseMessages.SUCCESSFULLY_CONNECTED);
        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.SUCCESSFULLY_CONNECTED);
    }

    @GetMapping(ApiPaths.SERVER_URL + "/configuration")
    public ResponseEntity configuration(){
        List<Object> properties = new ArrayList<>();
        properties.add(appProperties.getGeneralAppProperties());
        properties.add(appProperties.getPersistenceProperties());
        properties.add(appProperties.getCardsProperties());
        properties.add(appProperties.getEmailProperties());
        return UtilResponsesBuilder.buildOkResponseWithBody(properties);
    }

}
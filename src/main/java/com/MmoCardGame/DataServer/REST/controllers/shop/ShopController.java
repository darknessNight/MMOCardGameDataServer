package com.MmoCardGame.DataServer.REST.controllers.shop;

import com.MmoCardGame.DataServer.REST.jsonMessages.ProductInfo;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.shop.ShopService;
import com.MmoCardGame.DataServer.shop.products.Product;
import com.MmoCardGame.DataServer.shop.products.ProductCategories;
import com.MmoCardGame.DataServer.shop.products.ProductNames;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class ShopController {

    private static final Logger logger = LoggerFactory.getLogger(ShopController.class);

    private ShopService shopService;
    private UsersService usersService;

    @Autowired
    public ShopController(ShopService shopService, UsersService usersService){
        this.shopService = shopService;

        this.usersService = usersService;
    }

    @PutMapping(ApiPaths.USERS_URL + "/{login}/products")
    public ResponseEntity buyProduct(@PathVariable String login,
                                     @RequestBody ProductInfo productInfo){
        User user = usersService.getUserByLogin(login);
        String result = shopService.tryBuyProduct(user, productInfo.getName());
        usersService.updateUser(user);

        if(result.equals(ResponseMessages.PRODUCT_TO_EXPENSIVE)) {
            logger.warn("User tried to buy product but it is too expensive. login: {}, product: {}, product price {}, user money: {}",
                    login, productInfo.getName(), shopService.getProductPrice(productInfo.getName()), user.getMoney());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, result);
        }

        logger.info("Product successfully bought by user. login: {}, product: {}, product price {}, user money: {}",
                login, productInfo.getName(), shopService.getProductPrice(productInfo.getName()), user.getMoney());
        return UtilResponsesBuilder.buildOkResponseWithBody(result);
    }

    @GetMapping(ApiPaths.SHOP_URL + "/categories")
    public ResponseEntity getCategoriesList(){
        List<ProductCategories> productsCategories = shopService.getProductsCategories();
        logger.info("Returning products categories list: {}", productsCategories);
        return UtilResponsesBuilder.buildOkResponseWithBody(productsCategories);
    }

    @GetMapping(ApiPaths.SHOP_URL + "/products")
    public ResponseEntity getProducts(){
        List<Product> products = shopService.getProducts();
        logger.info("Returning products list: {}", products);
        return UtilResponsesBuilder.buildOkResponseWithBody(products);
    }

    @GetMapping(ApiPaths.SHOP_URL + "/{category}/products")
    public ResponseEntity getCategoryProducts(@PathVariable ProductCategories category){
        if(!Arrays.asList(ProductCategories.values()).contains(category)){
            logger.warn("Trying to get products of non existing category: {}", category);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.NO_SUCH_CATEGORY);
        }
        List<Product> productsOfCategory = shopService.getProductsOfCategory(category);
        logger.info("Returning products of category: {}, list: {}", category, productsOfCategory);
        return UtilResponsesBuilder.buildOkResponseWithBody(productsOfCategory);
    }

    @GetMapping(ApiPaths.SHOP_URL + "/products/{productName}")
    public ResponseEntity getProduct(@PathVariable ProductNames name){
        if(!Arrays.asList(ProductNames.values()).contains(name)){
            logger.warn("Trying to get non existing product: {}", name);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.NO_SUCH_PRODUCT);
        }
        Product product = shopService.getProduct(name);
        logger.info("Returning product by name: {}, product: {}", name, product);
        return UtilResponsesBuilder.buildOkResponseWithBody(product);
    }

}

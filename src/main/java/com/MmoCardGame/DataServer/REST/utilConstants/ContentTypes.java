package com.MmoCardGame.DataServer.REST.utilConstants;

import org.springframework.http.MediaType;

import java.nio.charset.Charset;

public class ContentTypes {
    public static final MediaType JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
}

package com.MmoCardGame.DataServer.REST.responses;

public class ResponseMessages {
    public static final String USER_WITH_LOGIN_NOT_EXISTS = "User with this login does not exist";
    public static final String PASSWORD_RESET = "Password successfully reset.";
    public static final String LOGGED_OUT = "Logged out";
    public static final String INVALID_TOKEN = "Invalid token.";
    public static final String NOT_ENOUGH_CARDS = "User doesn't have enough cards of this type.";
    public static final String TOKEN_REQUIRED = "X-Auth-Token header is required";

    public static final String DECK_DOES_NOT_EXIST = "Deck with given name does not exist.";
    public static final String CARD_DOES_NOT_EXIST = "Card with this id does not exist.";
    public static final String DECK_ALREADY_EXISTS = "Deck with this name already exists.";
    public static final String DECK_SELECTED = "Deck was successfully selected.";

    public static final String USER_HAVE_ZERO_PACKETS = "User doesn't have any packets.";
    public static final String PACKET_COULD_NOT_BE_OPENED = "Errors occurred during opening packets.";

    public static final String PRODUCT_BOUGHT = "Product has been successfully bought.";
    public static final String PRODUCT_TO_EXPENSIVE = "Product is to expensive.";
    public static final String NO_SUCH_CATEGORY = "Category does not exist.";
    public static final String NO_SUCH_PRODUCT = "Product does not exist.";

    public static final String GAME_START_REQUEST_APPROVED = "GamePlay start request approved.";
    public static final String USER_ALREADY_WAITING = "User is already waiting in some queue.";
    public static final String QUIT_FROM_QUEUE = "User is not in queue anymore.";
    public static final String NO_GAME_PLAY_FOR_USER = "Player is not in game play.";
    public static final String NO_GAME_PLAY_RESULT_FOR_USER = "Player doesn't have game play ressult.";
    public static final String USER_ALREADY_HAS_GAME = "User already has game play.";
    public static final String DECK_REMOVED = "Deck has been removed.";
    public static final String CANT_SELECT_DECK_WHILE_IN_QUEUE = "Can't select deck. User is waiting in queue.";
    public static final String CANT_SELECT_DECK_WHILE_IN_GAME = "Can't select deck. User is in game play.";
    public static final String SESSION_NOT_EXIST = "Session with given id does not exist.";

    public static final String SUCCESSFULLY_CONNECTED = "Successfully connected with server";
}

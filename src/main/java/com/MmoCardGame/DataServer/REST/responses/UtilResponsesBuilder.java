package com.MmoCardGame.DataServer.REST.responses;

import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.*;
import java.util.stream.Collectors;

public class UtilResponsesBuilder {

    public static ResponseEntity buildResponseWithMessage(HttpStatus status, String message){
        return ResponseEntity
                .status(status)
                .contentType(ContentTypes.JSON_UTF8)
                .body(message);
    }

    public static ResponseEntity buildBadRequestResponseWithErrors(List<String> errors) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(ContentTypes.JSON_UTF8)
                .body(errors);
    }

    public static ResponseEntity buildBadRequestResponseWithError(String error) {
        return buildBadRequestResponseWithErrors(Collections.singletonList(error));
    }

    public static ResponseEntity buildValidationErrorResponse(BindingResult bindingResult) {
        Set<String> validationErrors = bindingResult
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        List<String> errorsList = new ArrayList<>(validationErrors);
        Collections.sort(errorsList);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(ContentTypes.JSON_UTF8)
                .body(errorsList);
    }

    public static ResponseEntity buildCreatedResponse() {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(ContentTypes.JSON_UTF8)
                .build();
    }

    public static ResponseEntity buildOkResponse() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(ContentTypes.JSON_UTF8)
                .build();
    }

    public static ResponseEntity buildOkResponseWithBody(Object body) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(ContentTypes.JSON_UTF8)
                .body(body);
    }
}

package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GamePlayStartData {

    private GamePlayTypes gamePlayType;
    private String deckName;

}

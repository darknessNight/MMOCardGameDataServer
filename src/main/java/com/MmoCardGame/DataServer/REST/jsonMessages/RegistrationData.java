package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import com.MmoCardGame.DataServer.users.validation.email.ValidEmail;
import com.MmoCardGame.DataServer.users.validation.fieldsMatching.ValidFieldsMatching;
import com.MmoCardGame.DataServer.users.validation.login.ValidLogin;
import com.MmoCardGame.DataServer.users.validation.password.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidFieldsMatching(firstFieldName = "password",
        secondFieldName = "repeatedPassword",
        message = CredentialErrors.PASSWORDS_DONT_MATCH_MESSAGE)
public class RegistrationData {

    @ValidLogin
    private String login;

    @ValidPassword
    private String password;

    @ValidPassword
    private String repeatedPassword;

    @ValidEmail
    private String email;

}

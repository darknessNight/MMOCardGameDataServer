package com.MmoCardGame.DataServer.REST.jsonMessages;


import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import com.MmoCardGame.DataServer.users.validation.email.ValidEmail;
import com.MmoCardGame.DataServer.users.validation.fieldsMatching.ValidFieldsMatching;
import com.MmoCardGame.DataServer.users.validation.password.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidFieldsMatching(firstFieldName = "newEmail",
        secondFieldName = "repeatedEmail",
        message = CredentialErrors.EMAILS_DONT_MATCH_MESSAGE)
public class ChangeEmailData {

    @ValidEmail
    private String newEmail;

    @ValidEmail
    private String repeatedEmail;

    @ValidPassword
    private String password;

}

package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayStates;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UserGamePlayInfo {
    private String gamePlayId;
    private String login;
    private String authToken;
    private List<String> logins;
    private GamePlayTypes gamePlayType;
    private Date startDate;
    private GamePlayStates gamePlayState;
    private int sessionId;

    public UserGamePlayInfo(GamePlay gamePlay, String login){
        this.gamePlayId = gamePlay.getGameId();
        this.login = login;
        this.authToken = gamePlay.getAuthTokens().get(login);
        logins = new ArrayList<>();
        logins.addAll(gamePlay.getPlayers().keySet());
        this.gamePlayType = gamePlay.getGameType();
        this.startDate = gamePlay.getStartTime();
        this.gamePlayState = gamePlay.getState();
        this.sessionId = gamePlay.getSessionId();
    }

}

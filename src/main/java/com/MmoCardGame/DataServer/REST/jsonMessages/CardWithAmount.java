package com.MmoCardGame.DataServer.REST.jsonMessages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardWithAmount {

    private long cardId;

    private int amount;

}

package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import com.MmoCardGame.DataServer.users.validation.fieldsMatching.ValidFieldsMatching;
import com.MmoCardGame.DataServer.users.validation.password.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidFieldsMatching(firstFieldName = "newPassword",
        secondFieldName = "repeatedNewPassword",
        message = CredentialErrors.PASSWORDS_DONT_MATCH_MESSAGE)
public class ChangePasswordData {

    @ValidPassword
    private String newPassword;

    @ValidPassword
    private String repeatedNewPassword;

    @ValidPassword
    private String password;

}

package com.MmoCardGame.DataServer.REST.jsonMessages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GamePlayInfoForUser {

    private String login;
    private List<String> players;
    private Date startTime;
    private String deckName;
    private List<CardWithAmount> cards;
    private String gameToken;

}

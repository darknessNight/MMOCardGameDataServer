package com.MmoCardGame.DataServer.game;

import com.MmoCardGame.DataServer.REST.jsonMessages.EndGamePlayInfo;
import com.MmoCardGame.DataServer.game.results.GamePlayResult;
import com.MmoCardGame.DataServer.exceptions.UserIsAlreadyWaitingForGamePlay;
import com.MmoCardGame.DataServer.game.results.LastGamePlaysResultsService;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClient;
import com.MmoCardGame.DataServer.game.configuration.GameSettings;
import com.MmoCardGame.DataServer.game.data.*;
import com.MmoCardGame.DataServer.game.playersQueues.PlayersQueuesCollection;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class GamePlaysService {

    private static final Logger logger = LoggerFactory.getLogger(GamePlaysService.class);

    private GamePlaysCollection gamePlaysCollection;

    private PlayersQueuesCollection queuesCollection;

    private BattleServerClient battleServerClient;

    private UsersService usersService;

    private LastGamePlaysResultsService lastGamePlaysResults;

    @Autowired
    public GamePlaysService(GamePlaysCollection gamePlaysCollection,
                            PlayersQueuesCollection queuesCollection,
                            BattleServerClient battleServerClient,
                            UsersService usersService,
                            LastGamePlaysResultsService lastGamePlaysResults){
        this.gamePlaysCollection = gamePlaysCollection;
        this.queuesCollection = queuesCollection;
        this.battleServerClient = battleServerClient;
        this.usersService = usersService;
        this.lastGamePlaysResults = lastGamePlaysResults;
    }

    public void dispatchGameStartRequest(GamePlayStartRequest request){
        if(!queuesCollection.isUserWaiting(request.getUser()))
            queuesCollection.putToQueue(request.getGameType(), request.getUser());
        else
            throw new UserIsAlreadyWaitingForGamePlay(request.getUser().getLogin());
    }

    public boolean userHasGame(String login){
        return gamePlaysCollection.userHasGame(login);
    }

    public GamePlay getGamePlayForUser(String login){
        return gamePlaysCollection.getGamePlayForUser(login);
    }

    public boolean isUserWaiting(User user){
        return queuesCollection.isUserWaiting(user);
    }

    public void removeWaitingPlayer(User user){
        queuesCollection.removeWaitingPlayer(user);
    }

    public boolean sessionExist(int sessionId){
        return gamePlaysCollection.sessionExist(sessionId);
    }

    @Scheduled(fixedDelay = GameSettings.TIME_BETWEEN_GAME_PLAYS_CREATION)
    public void startAllPossibleGamePlays(){
        for(GamePlayTypes gameType : GamePlayTypes.values()) {
            GamePlay newGame;
            while(queuesCollection.canCreateGame(gameType)){
                newGame = queuesCollection.createGame(gameType);
                battleServerClient.sendGamePlayInfoToBattleServer(newGame);
                gamePlaysCollection.addGamePlay(newGame);
                logGamePlayCreation(newGame);
            }
        }
    }

    private void logGamePlayCreation(GamePlay newGame) {
        logger.info("========================================================");
        logger.info("New game play started and send to battle server.");
        logger.info("Session id     : {}", newGame.getSessionId());
        logger.info("Game type      : {}", newGame.getGameType());
        logger.info("Game id        : {}", newGame.getGameId());
        logger.info("Game state     : {}", newGame.getState());
        logger.info("Game start time: {}", newGame.getStartTime());
        logger.info("Users          :");
        for(User player:newGame.getPlayers().values()) {
            logger.info("     User login: {}", player.getLogin());
        }
        logger.info("========================================================");
    }

    public Map<String, GamePlay> getExistingGamePlays() {
        return gamePlaysCollection.getGamePlays();
    }

    public void handleEndGamePlayInfo(EndGamePlayInfo endGamePlayInfo) {
        int award = 0;
        if(!endGamePlayInfo.getResult().equals(GamePlayResults.KICKED)){
            award = awardPlayer(endGamePlayInfo);
        }
        GamePlay gamePlay = gamePlaysCollection.getGamePlay(endGamePlayInfo.getSessionId());
        gamePlay.endGamePlayByPlayer(endGamePlayInfo.getUserLogin());
        gamePlaysCollection.removePlayerFromGamePlay(endGamePlayInfo.getUserLogin());
        if(gamePlay.getState().equals(GamePlayStates.ENDED)){
            gamePlaysCollection.removeGamePlay(gamePlay.getGameId());
        }
        logger.info("Player {} ends game.", endGamePlayInfo.getUserLogin());
        GamePlayResult result = new GamePlayResult();
        result.setLogin(endGamePlayInfo.getUserLogin());
        result.setResult(endGamePlayInfo.getResult());
        result.setAward(award);
        lastGamePlaysResults.addResult(result);
    }

    private int awardPlayer(EndGamePlayInfo endGamePlayInfo) {
        int award = 0;
        User user = usersService.getUserByLogin(endGamePlayInfo.getUserLogin());
        if(endGamePlayInfo.getResult().equals(GamePlayResults.WIN)) {
            award = GameSettings.WINNER_AWARD;
            user.increaseMoney(award);
            logger.info("Awarding winner player: {}.", endGamePlayInfo.getUserLogin());
        } else if(endGamePlayInfo.getResult().equals(GamePlayResults.LOST)){
            award = GameSettings.LOOSER_AWARD;
            user.increaseMoney(award);
            logger.info("Awarding looser player: {}.", endGamePlayInfo.getUserLogin());
        }
        usersService.updateUser(user);
        return award;
    }

    public void removeAllGamePlays(){
        gamePlaysCollection.removeAllGamePlays();
    }
}

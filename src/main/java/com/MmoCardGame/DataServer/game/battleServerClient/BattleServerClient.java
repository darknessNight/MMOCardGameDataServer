package com.MmoCardGame.DataServer.game.battleServerClient;

import com.MmoCardGame.DataServer.game.data.GamePlay;

public interface BattleServerClient {
    void sendGamePlayInfoToBattleServer(GamePlay gamePlay);
}

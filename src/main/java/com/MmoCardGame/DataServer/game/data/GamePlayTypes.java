package com.MmoCardGame.DataServer.game.data;

import com.MmoCardGame.DataServer.exceptions.NoSuchGamePlayTypeException;

public enum GamePlayTypes {
    TWO_PLAYERS, THREE_PLAYERS, FOUR_PLAYERS, TEAM_GAME;

    public static final int SINGLE = 1;
    public static final int TEAM = 2;

    public static int getSingleOrTeamType(GamePlayTypes type){
        if(type.equals(TEAM_GAME)){
            return TEAM;
        } else if(type.equals(TWO_PLAYERS) || type.equals(THREE_PLAYERS) || type.equals(FOUR_PLAYERS)){
            return SINGLE;
        }
         throw new NoSuchGamePlayTypeException(type);
    }
}

package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import org.springframework.stereotype.Service;

@Service
public class TeamPlayersQueue extends PlayersQueueImpl {
    public TeamPlayersQueue() {
        super(4, GamePlayTypes.TEAM_GAME);
    }
}

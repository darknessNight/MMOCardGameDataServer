package com.MmoCardGame.DataServer.shop.products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private ProductNames name;

    private int price;

    private ProductCategories category;

}

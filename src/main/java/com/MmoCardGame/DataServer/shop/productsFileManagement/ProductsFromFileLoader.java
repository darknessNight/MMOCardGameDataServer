package com.MmoCardGame.DataServer.shop.productsFileManagement;

import com.MmoCardGame.DataServer.shop.products.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductsFromFileLoader implements ProductsLoader{

    private static final Logger logger = LoggerFactory.getLogger(ProductsFromFileLoader.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private String filePath;

    @Autowired
    public  ProductsFromFileLoader(@Qualifier("productsFilePath") String filePath){
        this.filePath = filePath;
    }

    @Override
    public List<Product> load() {

        List<Product> products = new ArrayList<>();
        try(FileReader fileReader = new FileReader(filePath)){
            products = objectMapper.readValue(fileReader, new TypeReference<List<Product>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        logProducts(products);
        return products;
    }

    private void logProducts(List<Product> products) {
        logger.info("Products loaded from file.");
        logger.info("File path: {}", filePath);
        for(Product product: products){
            logger.info("Product name: {}, category: {}, price: {}", product.getName(), product.getCategory(), product.getPrice());
        }
    }
}

package com.MmoCardGame.DataServer.appConfiguration.battleServer;

import lombok.Data;

@Data
public class BattleServerProperties {

    private Boolean useMockedBattleServer;
    private int battleServerPort;
    private String battleServerUrl;
    private String battleServerToken;

}

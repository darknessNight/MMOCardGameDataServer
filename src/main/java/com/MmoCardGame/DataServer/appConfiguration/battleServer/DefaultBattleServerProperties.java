package com.MmoCardGame.DataServer.appConfiguration.battleServer;

public class DefaultBattleServerProperties {

    public static final Boolean USE_MOCKED_BATTLE_SERVER = false;
    public static final int BATTLE_SERVER_PORT = 1914;
    public static final String BATTLE_SERVER_URL = "localhost";
    public static final String BATTLE_SERVER_TOKEN = "token";

}

package com.MmoCardGame.DataServer.appConfiguration;

import com.MmoCardGame.DataServer.appConfiguration.cards.CardsProperties;
import com.MmoCardGame.DataServer.appConfiguration.email.EmailProperties;
import com.MmoCardGame.DataServer.appConfiguration.battleServer.BattleServerProperties;
import com.MmoCardGame.DataServer.appConfiguration.persistence.PersistenceProperties;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@PropertySource(value = {"classpath:battleserver.properties",
                         "classpath:persistence.properties",
                         "classpath:mail.properties"})
public class AppProperties {

    private BattleServerProperties generalAppProperties;
    private CardsProperties cardsProperties;
    private PersistenceProperties persistenceProperties;
    private EmailProperties emailProperties;


    @Autowired
    public AppProperties(PropertiesLogger propertiesLogger,
                         BattleServerProperties generalAppProperties,
                         CardsProperties cardsProperties,
                         PersistenceProperties persistenceProperties,
                         EmailProperties emailProperties) {
        this.generalAppProperties = generalAppProperties;
        this.cardsProperties = cardsProperties;
        this.persistenceProperties = persistenceProperties;
        this.emailProperties = emailProperties;

        propertiesLogger.log(generalAppProperties);
        propertiesLogger.log(cardsProperties);
        propertiesLogger.log(persistenceProperties);
        propertiesLogger.log(emailProperties);

    }

}
package com.MmoCardGame.DataServer.appConfiguration.persistence;

import com.MmoCardGame.DataServer.exceptions.NoSuchPersistencePropertyNameException;

public enum PersistencePropertiesNames {

    USER,
    PASSWORD,
    CONNECTION_STRING,
    AUTO_POLICY;

    public static String getEnvName(PersistencePropertiesNames name) {
        switch(name){
            case USER:
                return "persistence_user";
            case PASSWORD:
                return "persistence_password";
            case CONNECTION_STRING:
                return "persistence_connectionString";
            case AUTO_POLICY:
                return "persistence_autoPolicy";
        }

        throw new NoSuchPersistencePropertyNameException(name.toString());
    }

    public static String getInFileName(PersistencePropertiesNames name) {
        switch(name){
            case USER:
                return "jdbc.user";
            case PASSWORD:
                return "jdbc.pass";
            case CONNECTION_STRING:
                return "jdbc.url";
            case AUTO_POLICY:
                return "hibernate.hbm2ddl.auto";
        }

        throw new NoSuchPersistencePropertyNameException(name.toString());
    }

}

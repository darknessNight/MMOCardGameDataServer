package com.MmoCardGame.DataServer.appConfiguration.persistence;

import lombok.Data;

@Data
public class PersistenceProperties {

    private String user;
    private String password;
    private String connectionString;
    private String autoPolicy;

}

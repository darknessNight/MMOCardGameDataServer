package com.MmoCardGame.DataServer.appConfiguration.persistence;

import com.MmoCardGame.DataServer.appConfiguration.PropertyLoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistencePropertiesLoader {
    private PropertyLoaderHelper propertyLoaderHelper;

    @Autowired
    public PersistencePropertiesLoader(PropertyLoaderHelper propertyLoaderHelper){
        this.propertyLoaderHelper = propertyLoaderHelper;
    }

    @Bean
    public PersistenceProperties persistenceProperties(){

        PersistenceProperties persistenceProperties = new PersistenceProperties();

        String user = propertyLoaderHelper.loadStringProperty(
                DefaultPersistenceProperties.USER,
                "persistence_user",
                "jdbc.user");
        persistenceProperties.setUser(user);

        String autoPolicy = propertyLoaderHelper.loadStringProperty(
                DefaultPersistenceProperties.AUTO_POLICY,
                "persistence_autoPolicy",
                "hibernate.hbm2ddl.auto");
        persistenceProperties.setAutoPolicy(autoPolicy);

        String password = propertyLoaderHelper.loadStringProperty(
                "",
                "persistence_password",
                "jdbc.pass");
        persistenceProperties.setPassword(password);

        String connectionString = propertyLoaderHelper.loadStringProperty(
                DefaultPersistenceProperties.CONNECTION_STRING,
                "persistence_connectionString",
                "jdbc.url");
        persistenceProperties.setConnectionString(connectionString);

        return persistenceProperties;
    }
}

package com.MmoCardGame.DataServer.appConfiguration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

@Data
@Configuration
@PropertySource("classpath:persistence.properties")
public class PersistenceConfiguration {

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        String className = environment.getProperty("jdbc.driverClassName");
        dataSource.setDriverClassName(className);
        String dataBaseUrl = appProperties.getPersistenceProperties().getConnectionString();
        dataSource.setUrl(dataBaseUrl);
        String dbUser = appProperties.getPersistenceProperties().getUser();
        dataSource.setUsername(dbUser);
        String dbPassword = appProperties.getPersistenceProperties().getPassword();
        dataSource.setPassword(dbPassword);

        return dataSource;
    }



    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(getPackagesToScan());

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    private String[] getPackagesToScan() {
        return new String[] {
                "com.MmoCardGame.DataServer.cards.cardsEntities",
                "com.MmoCardGame.DataServer.users" };
    }


    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", appProperties.getPersistenceProperties().getAutoPolicy());
        properties.setProperty(
                "hibernate.dialect", environment.getProperty("hibernate.dialect"));

        return properties;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}

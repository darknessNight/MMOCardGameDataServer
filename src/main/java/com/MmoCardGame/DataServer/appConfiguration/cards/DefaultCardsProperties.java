package com.MmoCardGame.DataServer.appConfiguration.cards;

import lombok.Data;

@Data
public class DefaultCardsProperties {

    public static final int NUMBER_OF_CARDS_IN_SMALL_PACKET = 3;
    public static final int NUMBER_OF_CARDS_IN_MEDIUM_PACKET = 5;
    public static final int NUMBER_OF_CARDS_IN_BIG_PACKET = 10;

    public static final int MIN_CARDS_IN_DECK = 45;
    public static final int MIN_LINK_CARDS_IN_DECK = 8;

}

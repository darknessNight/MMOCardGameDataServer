package com.MmoCardGame.DataServer.appConfiguration.email;

import lombok.Data;

@Data
public class EmailProperties {

    private String user;
    private String host;
    private String password;

}

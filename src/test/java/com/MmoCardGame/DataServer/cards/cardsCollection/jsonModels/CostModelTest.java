package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class CostModelTest {

    public ObjectMapper mapper;

    @Before
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void serializeCostModel_costModelShouldBeSerialized() throws JsonProcessingException {
        CostModel costModel = new CostModel();
        costModel.setNeutral(0);
        costModel.setPsychotronnics(1);
        costModel.setRaiders(2);
        costModel.setScanners(3);
        costModel.setWhiteHats(4);

        String costModelString = mapper.writeValueAsString(costModel);
        System.out.println(costModelString);
        assertEquals("{\"neutral\":0,\"raiders\":2,\"scanners\":3,\"whiteHats\":4,\"psychotronnics\":1}", costModelString);
    }

    @Test
    public void deserializeCostModel_costModelShouldBeDeserialized() throws IOException {
        String costModelString = "{\"neutral\":0,\"raiders\":2,\"scanners\":3,\"whiteHats\":4,\"psychotronnics\":1}";

        CostModel costModel = mapper.readValue(costModelString, CostModel.class);


        CostModel expected = new CostModel();
        expected.setNeutral(0);
        expected.setPsychotronnics(1);
        expected.setRaiders(2);
        expected.setScanners(3);
        expected.setWhiteHats(4);
        System.out.println(expected);
        assertEquals(expected, costModel);
    }


}
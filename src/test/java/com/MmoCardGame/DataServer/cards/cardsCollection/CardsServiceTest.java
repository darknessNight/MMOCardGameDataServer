package com.MmoCardGame.DataServer.cards.cardsCollection;


import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class CardsServiceTest {

    @Configuration
    static class CardsServiceTestContextConfiguration {
        @Bean
        public CardsService cardsService() {
            return new CardsService(cardsRepository());
        }
        @Bean
        public CardsRepository cardsRepository() {
            return Mockito.mock(CardsRepository.class);
        }
    }

    @Autowired
    private CardsService cardsService;
    @Autowired
    private CardsRepository cardsRepository;

    private List<Card> cards;
    private List<Card> connectionCards;

    @Before
    public void setup() {
        Card card1 = new Card(CardTypes.OTHER);
        Card card2 = new Card(CardTypes.LINK);
        Card card3 = new Card(CardTypes.LINK);
        connectionCards = new ArrayList<>();
        connectionCards.add(card2);
        connectionCards.add(card3);
        cards = new ArrayList<>();
        cards.add(card1);
        cards.addAll(connectionCards);
        Mockito.when(cardsRepository.findAll()).thenReturn(cards);
        Mockito.when(cardsRepository.count()).thenReturn((long) cards.size());
        Mockito.when(cardsRepository.findByType(CardTypes.LINK)).thenReturn(connectionCards);
    }

    @Test
    public void findByTypes_withConnectionAsParameterType_shouldReturnConnectionCards() throws Exception{
        List<Card> conCards = cardsService.getCardsByType(CardTypes.LINK);

        assertEquals(conCards.size(), connectionCards.size());
        assertEquals(conCards.get(0).getType(), CardTypes.LINK);
        assertEquals(conCards.get(1).getType(), CardTypes.LINK);
    }

}
package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class StatisticsModelTest {

    public ObjectMapper mapper;

    @Before
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void serializeStatisticsModel_statisticsModelShouldBeSerialized() throws JsonProcessingException {
        StatisticsModel statisticsModel = new StatisticsModel();
        statisticsModel.setAttack(10);
        statisticsModel.setDefence(20);

        String statisticsModelString = mapper.writeValueAsString(statisticsModel);
        System.out.println(statisticsModelString);
        assertEquals("{\"attack\":10,\"defence\":20}", statisticsModelString);
    }

    @Test
    public void deserializeStatisticsModel_statisticsModelShouldBeDeserialized() throws IOException {
        String statisticsModelString = "{\"attack\":10,\"defence\":20}";

        StatisticsModel statisticsModel = mapper.readValue(statisticsModelString, StatisticsModel.class);


        StatisticsModel expected = new StatisticsModel();
        expected.setAttack(10);
        expected.setDefence(20);

        System.out.println(expected);
        assertEquals(expected, statisticsModel);
    }

}
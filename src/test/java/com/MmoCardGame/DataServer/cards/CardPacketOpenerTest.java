package com.MmoCardGame.DataServer.cards;


import com.MmoCardGame.DataServer.cards.cardsCollection.CardsRepository;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class CardPacketOpenerTest {

    @Configuration
    static class AccountServiceTestContextConfiguration {
        @Bean
        public CardsService cardsService() {
            return new CardsService(cardsRepository());
        }
        @Bean
        public CardsRepository cardsRepository() {
            return Mockito.mock(CardsRepository.class);
        }
    }

    @Autowired
    private CardsService cardsService;
    @Autowired
    private CardsRepository cardsRepository;

    private Card card;

    @Before
    public void setup() {
        card = new Card(CardTypes.OTHER);
        List<Card> cards = new ArrayList<>();
        cards.add(card);
        Mockito.when(cardsRepository.findAll()).thenReturn(cards);
        Mockito.when(cardsRepository.count()).thenReturn((long) cards.size());
        Mockito.when(cardsRepository.findOne(card.getId())).thenReturn(card);
    }

    @Test
    public void openPacket_shouldReturnCards() throws Exception{
        CardsPacketOpener cardPacketOpener = new CardsPacketOpener(cardsService);

        List<Card> cardsFromPacket = cardPacketOpener.openPacket(PacketsTypes.MEDIUM_PACKET);

        assertEquals(cardsFromPacket.get(1).getType(), card.getType());
    }

}
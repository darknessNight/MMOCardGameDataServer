package com.MmoCardGame.DataServer.security;

import com.MmoCardGame.DataServer.security.users.SecurityUserWrapper;
import com.MmoCardGame.DataServer.users.User;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SecurityUserDetailsTest {

    private User user;

    @Before
    public void init(){
        String login = "login";
        String password = "password";
        String email = "email@poczta.pl";
        user = new User(login, password, email);
    }

    @Test
    public void getPassword() throws Exception {
        SecurityUserWrapper userDetails = new SecurityUserWrapper(user);

        String resultPassword = userDetails.getPassword();

        assertThat(resultPassword).isEqualTo(user.getPassword());
    }

    @Test
    public void getUsername() throws Exception {
        SecurityUserWrapper userDetails = new SecurityUserWrapper(user);

        String resultUsername = userDetails.getUsername();

        assertThat(resultUsername).isEqualTo(user.getLogin());
    }

}
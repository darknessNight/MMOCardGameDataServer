package com.MmoCardGame.DataServer.users.validation.email;

import org.junit.Test;
import org.mockito.Mock;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmailValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContextMock;

    @Test
    public void isValid_WithWrongStringAsEmail_shouldReturnFalse(){
        String invalidEmail = "asdasd";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    @Test
    public void isValid_WithNothingBeforeAt_shouldReturnFalse(){
        String invalidEmail = "@asd.asd";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    @Test
    public void isValid_WithNothingBetweenAtAndDot_shouldReturnFalse(){
        String invalidEmail = "asd@.asd";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    @Test
    public void isValid_WithNothingAfterDot_shouldReturnFalse(){
        String invalidEmail = "asd@asd.";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    @Test
    public void isValid_WithEmailWithoutAt_shouldReturnFalse(){
        String invalidEmail = "qweqwe.qwe";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithEmailWithoutDot_shouldReturnFalse(){
        String invalidEmail = "qwe@qweqwe";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithValidEmail_shouldReturnTrue(){
        String invalidEmail = "qwe@qwe.qwe";
        EmailValidator emailValidator = new EmailValidator();

        boolean validationResult = emailValidator.isValid(invalidEmail, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

}
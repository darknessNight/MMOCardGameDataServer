package com.MmoCardGame.DataServer.users.validation.login;

import org.junit.Test;
import org.mockito.Mock;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContextMock;

    @Test
    public void isValid_WithToShort2SignsLogin_shouldReturnFalse(){
        String invalidLogin = generateLogin(2);
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    @Test
    public void isValid_WithToLong21SignsLogin_shouldReturnFalse(){
        String invalidLogin = generateLogin(21);
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    private String generateLogin(int length) {
        StringBuilder loginBuilder = new StringBuilder();
        for(int i = 0; i < length; i++){
            loginBuilder.append("a");
        }
        return loginBuilder.toString();
    }

    @Test
    public void isValid_WithLoginWithPolishLetters_shouldReturnFalse(){
        String invalidLogin = "qweą";
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithLoginWithRestrictedSymbol_shouldReturnFalse(){
        String invalidLogin = "asda-";
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithValid3SignLogin_shouldReturnTrue(){
        String invalidLogin = generateLogin(3);
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

    @Test
    public void isValid_WithNormalLogin_shouldReturnTrue(){
        String invalidLogin = "kaszub300";
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

    @Test
    public void isValid_WithValid20SignLogin_shouldReturnTrue(){
        String invalidLogin = generateLogin(20);
        LoginValidator LoginValidator = new LoginValidator();

        boolean validationResult = LoginValidator.isValid(invalidLogin, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

}
package com.MmoCardGame.DataServer.users;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsRepository;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import com.MmoCardGame.DataServer.cards.cardsEntities.UserCard;
import com.MmoCardGame.DataServer.cards.cardsEntities.UserCardId;
import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@ContextConfiguration
public class UserTest {

    @Configuration
    static class UserTestContextConfiguration {
        @Bean
        public CardsService cardsService() {
            return new CardsService(cardsRepository());
        }
        @Bean
        public CardsRepository cardsRepository() {
            return Mockito.mock(CardsRepository.class);
        }
        @Bean
        public UsersService usersService(){return new UsersService();}
        @Bean
        public UsersRepository usersRepository(){return Mockito.mock(UsersRepository.class);}
        @Bean
        public CardsPacketOpener cardPacketOpener(){return new CardsPacketOpener(cardsService());}
    }

    @Autowired
    private CardsService cardsService;
    @Autowired
    private CardsRepository cardsRepository;
    @Autowired
    private UsersService usersService;
    @Autowired
    private UsersRepository usersRepository;

    private List<Card> cards;
    private List<User> users;
    private User existingRandomUser = new User("randomLogin", "randomPassword", "email@email.com");


    @Before
    public void setup() {
        Card card1 = new Card(CardTypes.OTHER);
        card1.setId(1L);
        Card card2 = new Card(CardTypes.LINK);
        card2.setId(2L);
        Card card3 = new Card(CardTypes.LINK);
        card3.setId(3L);
        cards = new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);

        users = new ArrayList<>();
        users.add(existingRandomUser);

        Mockito.when(cardsRepository.findAll()).thenReturn(cards);
        Mockito.when(usersRepository.findByLogin(existingRandomUser.getLogin())).thenReturn(existingRandomUser);
    }

    @Test
    public void addCard_whenUserHasZeroOfThisCard_shouldAddAssociationWithOneCard(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());

        user.addCard(cards.get(0));

        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        assertEquals(updatedUser.getCards().size(), 1);
        assertEquals(updatedUser.getCards().iterator().next().amount, 1);
    }

    @Test
    public void addTwoSameCards_whenUserHasZeroOfThisCard_shouldAddAssociationWithThisCardAndSetItsValueToTwo(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());

        user.addCard(cards.get(0));
        user.addCard(cards.get(0));

        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        assertEquals(updatedUser.getCards().size(), 1);
        assertEquals(updatedUser.getCards().iterator().next().amount, 2);
    }

    @Test
    public void addCard_whenUserAlreadyHaveAssociationWithThisCard_shouldNotAddAssociationWithCard(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        UserCard userCard = new UserCard(user, cards.get(0));
        cards.get(0).getUsers().add(userCard);
        user.getCards().add(userCard);

        user.addCard(cards.get(0));

        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        assertEquals(updatedUser.getCards().size(), 1);
    }

    @Test
    public void addCard_whenUserHasOneOfThisCard_shouldIncreaseCardAmount(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        UserCard userCard = new UserCard(user, cards.get(0));
        cards.get(0).getUsers().add(userCard);
        user.getCards().add(userCard);

        user.addCard(cards.get(0));

        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        assertEquals(updatedUser.getCards().iterator().next().amount, 2);
    }

    @Test
    public void getCards_whenUserHasNoCards_shouldReturnEmptyList(){

        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        assertEquals(user.getCards().size(), 0);
    }

    @Test
    public void getCards_whenUserHasTwoDifferentCardsOneOfFirstTypeAndTwoOfSecondType_shouldReturnProperResult(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        //adding first card
        UserCard firstCard = new UserCard(user, cards.get(0));
        UserCardId userCardId1 = new UserCardId();
        userCardId1.setCardId(1L);
        userCardId1.setUserId(1L);
        firstCard.setId(userCardId1);
        cards.get(0).getUsers().add(firstCard);
        user.getCards().add(firstCard);
        //adding second card
        UserCard secondCard = new UserCard(user, cards.get(1));
        UserCardId userCardId2 = new UserCardId();
        userCardId2.setCardId(2L);
        userCardId2.setUserId(1L);
        secondCard.setId(userCardId2);
        secondCard.increaseAmount();
        cards.get(0).getUsers().add(secondCard);
        user.getCards().add(secondCard);


        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        Set<UserCard> cardsAssociations = updatedUser.getCards();
        Iterator<UserCard> iterator = cardsAssociations.iterator();
        assertEquals(iterator.next().getAmount(), 1);
        assertEquals(iterator.next().getAmount(), 2);
        assertEquals(cardsAssociations.size(), 2);
    }

    @Test
    public void addCardsOfTwoTypes_whenUserHasTwoCardsOfFirstTypeAndZeroOfSecondType_shouldResultWithProperResult(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        //adding first card
        Card card1 = cards.get(0);
        UserCard firstCard = new UserCard(user, card1);
        UserCardId userCardId1 = new UserCardId();
        userCardId1.setCardId(card1.getId());
        userCardId1.setUserId(user.getId());
        firstCard.setId(userCardId1);
        firstCard.increaseAmount();
        card1.getUsers().add(firstCard);
        user.getCards().add(firstCard);

        user.addCard(card1);
        user.addCard(cards.get(1));

        User updatedUser = usersService.getUserByLogin(existingRandomUser.getLogin());
        Set<UserCard> cardsAssociations = updatedUser.getCards();
        Iterator<UserCard> iterator = cardsAssociations.iterator();
        assertEquals(1, iterator.next().getAmount());
        assertEquals(3, iterator.next().getAmount());
        assertEquals(2, cardsAssociations.size());
    }

    @Test
    public void selectDeck_default_shouldChangeSelectedDeckName(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        String deckName = "default";

        user.setSelectedDeckName(deckName);

        assertEquals(user.getSelectedDeckName(), deckName);
    }

    @Test
    public void selectDeck_default_shouldChangeSelectedDeck(){
        User user = usersService.getUserByLogin(existingRandomUser.getLogin());
        Deck deck = new Deck();
        deck.setName("default");
        Card card = cards.get(0);
        user.addCard(card);
        deck.addCard(card);
        user.addDeck(deck);
        String deckName = "default";

        user.setSelectedDeckName(deckName);

        assertEquals(deckName, user.getSelectedDeck().getName());
        assertEquals(1, user.getSelectedDeck().getCards().size());
    }

}
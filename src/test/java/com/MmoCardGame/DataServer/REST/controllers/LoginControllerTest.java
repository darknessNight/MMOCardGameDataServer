package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.REST.utilConstants.HeaderNames;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenService;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import sun.misc.BASE64Encoder;

import javax.servlet.Filter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class LoginControllerTest {

    private MockMvc mockMvc;

    private List<User> users = new ArrayList<>();
    private String token = "24i8xcv0-ad8c-vzvd-9zcx-qweg98dxcv8d";
    private static String TOKEN_REGEX = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UsersController controller;

    @Autowired
    private TokenService tokenServiceMock;

    @Autowired
    private UsersRepository usersRepositoryMock;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(webApplicationContext)
                            .addFilters(springSecurityFilterChain)
                            .build();

        usersRepositoryMock.deleteAll();

        User user1 = new User(
                "qweqweqwe",
                passwordEncoder.encode("qweqweqwe"),
                "qweqweqwe@qweqweqwe.qweqweqwe");
        usersRepositoryMock.save(user1);
        users.add(user1);
        User user2 = new User(
                "asdasdasd",
                passwordEncoder.encode("asdasdasd"),
                "asdasdasd@asdasdasd.asdasdasd");
        usersRepositoryMock.save(user2);
        users.add(user2);

        Authentication authentication = new UsernamePasswordAuthenticationToken(user1, null);
        tokenServiceMock.removeAll();
        tokenServiceMock.store(token, authentication);

    }

    private String getAuthorizationHeader(String login, String password) {
        return "Basic " + new BASE64Encoder().encode((login + ":" + password).getBytes());
    }

    @Test
    public void loginInvalidUser_shouldReturnUnauthorized401Response() throws Exception{
        String login = "invalid";
        String password = "invalid";

        mockMvc.perform(post(ApiPaths.LOGGED_USERS_URL)
                    .header("Authorization", getAuthorizationHeader(login, password)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginUserWithEmptyLoginAndPassword_shouldReturnUnauthorized401Response() throws Exception{
        String login = "";
        String password = "";

        mockMvc.perform(post(ApiPaths.LOGGED_USERS_URL)
                .header("Authorization", getAuthorizationHeader(login, password)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginUser_shouldReturnOk200ResponseAndRandomToken() throws Exception{
        String login = "asdasdasd";
        String password = "asdasdasd";

        MvcResult result = mockMvc.perform(post(ApiPaths.LOGGED_USERS_URL)
                .header("Authorization", getAuthorizationHeader(login, password)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andReturn();

        String resultString = result.getResponse().getContentAsString();
        String tokenString = resultString.split("\"")[3];
        assertTrue(tokenString.matches(TOKEN_REGEX));
    }

    @Test
    public void logoutUser_shouldReturnOk200ResponseAndProperMessage() throws Exception{
        String login = "asdasdasd";
        String password = "asdasdasd";

        mockMvc.perform(delete(ApiPaths.LOGGED_USERS_URL)
                .header(HeaderNames.TOKEN_HEADER_NAME, token)
                .header("Authorization", getAuthorizationHeader(login, password)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(content().string(ResponseMessages.LOGGED_OUT));
    }

    @Test
    public void logoutUser_shouldRemoveTokenFromTokenService() throws Exception{
        String login = "asdasdasd";
        String password = "asdasdasd";

        mockMvc.perform(delete(ApiPaths.LOGGED_USERS_URL)
                .header(HeaderNames.TOKEN_HEADER_NAME, token)
                .header("Authorization", getAuthorizationHeader(login, password)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(content().string(ResponseMessages.LOGGED_OUT));

        assertFalse(tokenServiceMock.contains(token));
    }

    @Test
    @WithMockUser(username = "userLogin", password = "userLogin", roles = "USER")
    public void logoutUserWithInvalidToken_shouldReturnUnauthorized401Response() throws Exception{
        String token = "qwe";
        mockMvc.perform(delete(ApiPaths.LOGGED_USERS_URL)
                .header(HeaderNames.TOKEN_HEADER_NAME, token))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "userLogin", password = "userLogin", roles = "USER")
    public void logoutUserWithoutToken_shouldReturnUnauthorized401Response() throws Exception{
        mockMvc.perform(delete(ApiPaths.LOGGED_USERS_URL))
                .andExpect(status().isUnauthorized());
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, ContentTypes.JSON_UTF8, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}

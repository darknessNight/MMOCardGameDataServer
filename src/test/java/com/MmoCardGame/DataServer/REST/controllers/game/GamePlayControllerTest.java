package com.MmoCardGame.DataServer.REST.controllers.game;

import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.game.GamePlaysService;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClient;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayResults;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.game.playersQueues.PlayersQueuesCollection;
import com.MmoCardGame.DataServer.game.results.GamePlayResult;
import com.MmoCardGame.DataServer.game.results.LastGamePlaysResultsService;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.users.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ActiveProfiles("battleServerTest")
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class GamePlayControllerTest {

    private MockMvc mockMvc;

    private User randomUser;

    private List<User> mockedUsers;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private GamePlaysService gamePlaysService;

    @Autowired
    private PlayersQueuesCollection queuesCollection;

    @Autowired
    private GamePlayController gamePlayController;

    @Autowired
    private UsersService usersService;

    @Autowired
    private BattleServerClient battleServerClient;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private LastGamePlaysResultsService lastGamePlaysResultsService;

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        usersRepository.deleteAll();

        randomUser = new User("qwe", "qwe", "qwe");
        usersService.addUser(randomUser);

        mockedUsers = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            String seed = "qwe" + i;
            User user = new User(seed, seed, seed);
            mockedUsers.add(user);
            usersService.addUser(user);
        }

        gamePlaysService.getExistingGamePlays().clear();

    }

    @Test
    public void requestForGameStart_shouldReturnOk200Response() throws Exception {
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + randomUser.getLogin() +"/game/" + gameType))
            .andExpect(status().isOk());

    }

    @Test
    public void requestForGameStart_shouldAddPlayerToProperQueue() throws Exception {
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + randomUser.getLogin() +"/game/" + gameType));

        assertTrue(queuesCollection.getQueue(gameType).contains(randomUser));
    }

    @Test
    public void creatingAllPossibleGames_afterQueueingEnoughPlayersForTwoGames_shouldCreateTwoGames() throws Exception{
        GamePlayTypes gameType = GamePlayTypes.TWO_PLAYERS;
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + mockedUsers.get(0).getLogin() +"/game/" + gameType));
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + mockedUsers.get(1).getLogin() +"/game/" + gameType));
        GamePlayTypes gameType2 = GamePlayTypes.THREE_PLAYERS;
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + mockedUsers.get(2).getLogin() +"/game/" + gameType2));
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + mockedUsers.get(3).getLogin() +"/game/" + gameType2));
        mockMvc.perform(post(ApiPaths.USERS_URL + "/" + mockedUsers.get(4).getLogin() +"/game/" + gameType2));

        gamePlaysService.startAllPossibleGamePlays();

        Map<String, GamePlay> existingGamePlays = gamePlaysService.getExistingGamePlays();
        assertEquals(2, existingGamePlays.size());
    }

    @Test
    public void requestForDeletingFromQueue_shouldDeletePlayerFromQueue() throws Exception {
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        queuesCollection.putToQueue(gameType, randomUser);

        mockMvc.perform(delete(ApiPaths.USERS_URL + "/" + randomUser.getLogin() +"/game"));

        assertFalse(queuesCollection.getQueue(gameType).contains(randomUser));
    }

    @Test
    public void getLastGamePlayResult_withExistingResult_shouldReturnOk200Response() throws Exception{
        String login = "user";
        GamePlayResult result = new GamePlayResult();
        result.setAward(1000);
        result.setLogin(login);
        result.setResult(GamePlayResults.WIN);
        lastGamePlaysResultsService.addResult(result);

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + login + "/game/lastResult"))
                .andExpect(status().isOk());

    }

    @Test
    public void getLastGamePlayResult_withoutExistingResult_shouldReturnNotFound() throws Exception{
        String login = "user1";

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + login + "/game/lastResult"))
                .andExpect(status().isNotFound());

    }

    @Test
    public void getLastGamePlayResult_withExistingResult_shouldReturnProperResult() throws Exception{
        String login = "user";
        GamePlayResult result = new GamePlayResult();
        result.setAward(1000);
        result.setLogin(login);
        result.setResult(GamePlayResults.WIN);
        lastGamePlaysResultsService.addResult(result);

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + login + "/game/lastResult"))
                .andExpect(jsonPath("$.login", is(login)))
                .andExpect(jsonPath("$.award", is(1000)))
                .andExpect(jsonPath("$.result", is(GamePlayResults.WIN.toString())));

    }
}
package com.MmoCardGame.DataServer.REST.controllers.game;

import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClient;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClientMock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("battleServerTest")
@Configuration
public class BattleServerTestConfiguration {

    @Bean
    @Primary
    public BattleServerClient battleServerClient(){
        return new BattleServerClientMock();
    }
}

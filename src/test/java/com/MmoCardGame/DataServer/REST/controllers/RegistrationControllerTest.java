package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.RegistrationData;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class RegistrationControllerTest {

    private String randomLogin = "zxczxc";
    private String randomPassword = "zxczxc123";
    private String randomMisspelledPassword = "zxczxc12";
    private String randomEmail = "zxc@zxc.zxc";

    private MockMvc mockMvc;

    private List<User> users = new ArrayList<>();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        usersRepository.deleteAll();

        User user1 = new User(
                "qwe",
                passwordEncoder.encode("qwe1234"),
                "qwe@qwe.qwe");
        usersRepository.save(user1);
        users.add(user1);
        User user2 = new User(
                "asd",
                passwordEncoder.encode("asd1234"),
                "asd@asd.asd");
        usersRepository.save(user2);
        users.add(user2);
    }

    @Test
    public void registerUserWithUsedEmail_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, randomPassword, randomPassword, "qwe@qwe.qwe");
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.EMAIL_USED_MESSAGE));
    }

    @Test
    public void registerUserWithInvalidData_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String invalidRegistrationData = "invalid";
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(invalidRegistrationData))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerUserWithNullLogin_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData(null, randomPassword, randomPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.INVALID_LOGIN_MESSAGE));
    }

    @Test
    public void registerUserWithUsedLogin_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData("qwe", randomPassword, randomPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.LOGIN_USED_MESSAGE));
    }

    @Test
    public void registerUserWithNotValidEmail_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, randomPassword, randomPassword, "zxc@zxc");
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.INVALID_EMAIL_MESSAGE));
    }

    @Test
    public void registerUserWithNotValidLogin_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData("a", randomPassword, randomPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.INVALID_LOGIN_MESSAGE));
    }

    @Test
    public void registerUserWithNotValidPassword_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, "a", "a", randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.INVALID_PASSWORD_MESSAGE));
    }

    @Test
    public void registerUserWithNotMatchingPasswords_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, randomPassword, randomMisspelledPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.PASSWORDS_DONT_MATCH_MESSAGE));
    }

    @Test
    public void registerUserWithNotValidPasswordLoginAndEmail_shouldReturnBadRequest400ResponseAndAlphabeticalSortedMessages() throws Exception{
        RegistrationData registrationData = new RegistrationData("c", "a", "a", "asd");
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$[0]").value(CredentialErrors.INVALID_EMAIL_MESSAGE))
                .andExpect(jsonPath("$[1]").value(CredentialErrors.INVALID_LOGIN_MESSAGE))
                .andExpect(jsonPath("$[2]").value(CredentialErrors.INVALID_PASSWORD_MESSAGE));
    }

    @Test
    public void registerUser_shouldReturnCreated201Response() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, randomPassword, randomPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void registerUser_shouldAddNewUserToUsersRepository() throws Exception{
        RegistrationData registrationData = new RegistrationData(randomLogin, randomPassword, randomPassword, randomEmail);
        String registrationDataJson = json(registrationData);
        mockMvc.perform(post(ApiPaths.USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(registrationDataJson))
                .andExpect(status().isCreated());

        User newUser = usersRepository.findByLogin(randomLogin);

        assertThat(newUser.getLogin()).isEqualTo(randomLogin);
        assertTrue(passwordEncoder.matches(randomPassword, newUser.getPassword()));
        assertThat(newUser.getEmail()).isEqualTo(randomEmail);
        assertThat(usersRepository.findAll().size()).isEqualTo(3);
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, ContentTypes.JSON_UTF8, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
